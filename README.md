# portfolio
[![Netlify Status](https://api.netlify.com/api/v1/badges/23f73463-4e96-4f20-a430-c6d6988e5513/deploy-status)](https://app.netlify.com/sites/eager-ptolemy-0feb38/deploys)
> My awesome Nuxt.js project

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
